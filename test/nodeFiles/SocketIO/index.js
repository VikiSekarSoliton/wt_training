var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection',function(socket){
    console.log('A user connected with session id - ' + socket.id);

    socket.on('chat message', function(msg){
        console.log('message: ' + msg);
        io.emit('chat message', msg);
    });

    socket.on('disconnect',function(){
        console.log('User Disconnected - ' + socket.id);
    });
});

http.listen(5000, function(){
  console.log('listening on *:5000');
});