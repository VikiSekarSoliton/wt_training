var http = require('http');
var url = require('url');

console.log("Creating a server on http://localhost:8080/ \nTry the following urls :" + 
"\nhttp://localhost:8080/someLink\nhttp://localhost:8080/?firstName=FIRSTNAME&lastName=LASTNAME");
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Hello World!');
    res.write('\nRequested URL - ' + req.url);
    var q = url.parse(req.url, true).query;
    var txt;
    if(!q.firstName){
        txt = "not yet arrived..";
    }else{
        txt = q.firstName + " " + q.lastName;
    }
    
    res.write("\nMy Name is " + txt);
    res.end();
}).listen(8080);


