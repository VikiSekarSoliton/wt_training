var spawn = require('child_process').spawn;
var fork = require('child_process').fork;
console.log("---------------------------------SPAWN----------------------------");
//SPAWN - Execute system processes that run in a different thread
var child = spawn(process.execPath,['test.js']);

child.on('exit', function (code, signal) {
    console.log('spawn process exited');
    
    startFork();
});

child.stdout.on('data',function(data){
    console.log("Child stdout Data - \n" + data);
});

child.stderr.on('data',function(data){
    console.log("Child stderr Data - \n" + data);
});


/*FORK - to run node process with communication channels/event emitters - Very helpful in redirecting
long running tasks to a different process and get the result back when complete, so that server can handle
other requests without being blocked*/

function startFork(){
    forkProcess = fork('child.js');
    
    forkProcess.send({status:'Child Process Started',isChild:false});
    
    forkProcess.on('message',function(msg){
        console.log('Child Says - ' + msg.status);
        process.exit();
    });

}


