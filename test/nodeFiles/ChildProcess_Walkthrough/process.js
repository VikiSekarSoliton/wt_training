var fs = require('fs');
var fileName = 'file.txt';
var exec = require('child_process').exec;


console.log("Arguments passed in cmd : " + process.argv);

process.stdout.write("Msg written from process.stdout.write [FROM CODE]\n");

fs.writeFileSync(fileName,"I am written using process.stdout!! [FROM FILE]\n");
fs.createReadStream(fileName).pipe(process.stdout);

exec('type demo.txt',function(err, stdout, stderr){
    console.log("The file contents from exec(creates a shell to execute commands) are : " + stdout);
});

//Send message to parent
process.send({onExit:"I'm done with my tasks.."});

