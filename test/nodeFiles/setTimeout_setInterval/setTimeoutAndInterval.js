var count = 10;
console.log("---------------------------------Set Timeout----------------------------");
//Executes only once after defined timeout - Should be carefully used inside for loop
for(var i = 0; i<=5;i++){
    setTimeout(function(){
        console.log("My i value without enclosing is - " + i);
    },1000);
}

for(var i = 0; i<=5;i++){
    (function(i){
        setTimeout(function(){
            console.log("My i value by enclosing is - " + i);
        },1000);
    } )(i);  
}

setTimeout(function(){
    console.log("---------------------------------Set Interval----------------------------");
    console.log("Hi.. I am going to shut down after 10 to 1!! [setInterval]");
},1000);

//Executes for each 1 second from start until we force quit
setInterval(function(){
    console.log(count--);
    if(count < 1){
        console.log("Happy Coding !!");
        process.exit();
    }
},1000);