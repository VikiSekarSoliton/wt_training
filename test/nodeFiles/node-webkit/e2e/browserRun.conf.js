exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['checkCategorySelectSpec.js'],
    multiCapabilities:[{
      browserName:'chrome'
    },{
      browserName:'firefox'
    }]
  }