var libraryPage = function(){
    var chkTechnicalBooks = element.all(by.model('category.isSelected')).first();
    var chkNonTechnicalBooks = element.all(by.model('category.isSelected')).last();
    var techBooksTable = element(by.id('Technical Books'));
    var NonTechBooksTable = element(by.id('Non-Technical Books'));
    var txtCopies = element.all(by.binding('book.copies')).first();
    var btnAddToCart = element.all(by.name('Add to Cart')).first();
    var cartTable = element(by.id('cartTable'));

    this.clickChkTechnicalBooks = function(){
        chkTechnicalBooks.click();
    };

    this.getCopies = function(){
        return txtCopies.getText();
    };

    this.getCartTable = function(){
        return cartTable;
    };

    this.clickChkNonTechnicalBooks = function(){
        chkNonTechnicalBooks.click();
    };

    this.clickBtnAddToCart = function(){
        btnAddToCart.click();
    };

    this.getTechBooksTable = function(){
        return techBooksTable;
    };

    this.getNonTechBooksTable = function(){
        return NonTechBooksTable;
    };

    

};

module.exports.libraryPage = libraryPage;