const path = require('path');

exports.config = {
    chromeDriver: './../node-webkit-chromedriver/chromedriver.exe', // relative path to node-webkit's chromedriver
    chromeOnly: true, // starting Selenium server isn't required in our case
    specs: ['checkLibrarySpec.js'],
    //baseUrl: 'file:///C:/AngUnitTest/WT_training/test/nodeFiles/node-webkit/Assignment-4a/index.html',
    rootElement: 'body',

    onPrepare: function() {
              browser.driver.get(path.resolve('./../Assignment-4a/index.html'));
            }
  };