var pageObj = require('./libraryPageObject').libraryPage;

describe('Digital Library',function(){
    var page = new pageObj();

        it('should show respective categories when box is checkmarked',function(done){
           page.clickChkTechnicalBooks();
           page.clickChkNonTechnicalBooks();
           expect(page.getTechBooksTable().isDisplayed()).toBe(true);
           expect(page.getNonTechBooksTable().isDisplayed()).toBe(true);
           done();
        });

        it('should add item to cart',function(done){
            page.getCopies().then(function(copies){
                originalCopies = Number(copies);
                page.clickBtnAddToCart();
                expect(page.getCartTable().isDisplayed()).toBe(true);
            }).then(function (){
                    page.getCopies().then(function(copies){
                    newCopies = Number(copies);
                    expect(--originalCopies).toBe(newCopies);
                    done();
                }); 
            });
         });

        afterEach(function(){
            browser.sleep(1000);
        });

});