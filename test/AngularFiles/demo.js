function demo(){
    var books = [{name:'Book1'},{name:'Book2'}];
    
    return {
        addToCart:function(){
            console.log('Added to cart');
        },
        addBook:function(book){
            books.push(book);
        },
        getBooks:function(){
            return books;
        }
    };
}

var anotherDemo = demo();
var demo = demo();

demo.addToCart();
console.log(demo.getBooks());
demo.addBook({name:'Book3'});
console.log(demo.getBooks());


console.log(anotherDemo.getBooks());