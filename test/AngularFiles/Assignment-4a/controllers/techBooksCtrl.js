libraryApp.controller('techCtrl',['$scope','cartService',techCtrl]);

function techCtrl($scope,cartService){
    $scope.books = [{name:"Introduction to C",copies:4},
                     {name:"AngularJS by Misko Hevery", copies:1},
                     {name:"Introduction to Node.JS",copies:0}];
     
     $scope.$on('book-returned',function(event,returnedBook){
         cartService.refreshInventory($scope.books,event,returnedBook);
     });
 }