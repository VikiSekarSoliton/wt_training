
libraryApp.controller('nonTechCtrl',['$scope','cartService',nonTechCtrl]);


function nonTechCtrl($scope,cartService){
    $scope.books = [{name:"Wings of Fire - Dr.APJ Abdul Kalam",copies:4},
                    {name:"A hundred hundreds - Sachin", copies:1},
                    {name:"Spirit Of Music - ARR",copies:5}];
    
    $scope.$on('book-returned',function(event,returnedBook){
        cartService.refreshInventory($scope.books,event,returnedBook);
    });
}