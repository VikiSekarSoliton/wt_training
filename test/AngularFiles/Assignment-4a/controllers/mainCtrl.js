
libraryApp.controller('mainCtrl',['$scope','cartService',function($scope,cartService){
    $scope.categories = [{name:'Technical Books',isSelected:false,controller:techCtrl},
    {name:'Non-Technical Books',isSelected:false,controller:nonTechCtrl}];

    $scope.cartItems = [];

    $scope.addToCart = function(book){
        cartService.addToCart($scope.cartItems,book);
    };
    
    $scope.removeFromCart = function(book,$index){
        cartService.removeFromCart($scope.cartItems,book,$index);
        
        $scope.$broadcast('book-returned',book);
    };

    $scope.checkOut = function(){
        $scope.cartItems = [];
        cartService.checkOut($scope.cartItems);
    };
        
}]);


