app.controller('mainCtrl',['$rootScope','$scope','DataService','ErrorMsgService',function($rootScope,
    $scope,DataService,ErrorMsgService){
    
    $scope.primaryInfo = {};
    
    $scope.cities = DataService.cities;
    $scope.cityInputErrors = ErrorMsgService.cityInput;

    $scope.fetchData = function(){
        //Send primaryInfo data to service and fetch the details as if in request to server
        DataService.fetchBusData().then(function(buses){
            console.log('I got the data - ',buses);
        });

    };
    
    $scope.toggleCities = function(){
        console.log('Toggling..');
        [$scope.primaryInfo.fromCity,$scope.primaryInfo.toCity] = [$scope.primaryInfo.toCity,$scope.primaryInfo.fromCity]
    };

    $scope.pushSelection = function(city){
        if(!$scope.primaryInfo.fromCity){
            $scope.primaryInfo.fromCity = city;
        }else{
            $scope.primaryInfo.toCity = city;
        }
    };
    
}]);

