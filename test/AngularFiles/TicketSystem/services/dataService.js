app.service('DataService',['$q','$timeout',function($q,$timeout){
    this.cities = ('Chennai Coimbatore Madurai Erode Salem Thoothukudi Tiruchirappalli ' + 
    'Tirunelveli Thanjavur Tiruppur Vellore Dindigul').split(' ');


    this.fetchBusData = function(primaryInfo){
        var deferred = $q.defer();
        var buses = [{type:'Semi-Sleeper',capacity:'50'},{type:'Sleeper',capacity:'50'}];

        deferred.notify('Started request..');

        $timeout(function(){
            deferred.resolve(buses);
        },2000);

        return deferred.promise;
    };

}]);
