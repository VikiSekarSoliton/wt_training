var app = angular.module('TicketApp',['ngMaterial','ngRoute','ngMessages']);


app.config(function($mdThemingProvider,$routeProvider){
    $mdThemingProvider.theme('docs-dark','default')
        .primaryPalette('red')
        .dark();
});