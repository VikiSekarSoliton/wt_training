var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var urlEncodedParser = bodyParser.urlencoded({extended:false});

app.use(express.static(path.join(__dirname + '/../client')));


app.get('/',function(req,res){
    res.sendFile(path.resolve(__dirname + "/../client/views/index.html"));  
});



/*  app.post('/process_post',urlEncodedParser,function (req, res) {  
     
});

 app.get('/process_get', function (req, res) {  
 
 }); */


var server = app.listen(8000,function(){
    console.log("Server listening now on " + server.address().port);
    console.log("Ctrl+C to stop!!");
    console.log("Try the following URLs : \nhttp://localhost:8000/");
});