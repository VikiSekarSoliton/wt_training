app.service('NavigationService',['$location',function($location){
    this.goTo = function(page,partial,scope){
        $location.path(partial);
        scope.currentNavItem = page;
    };
}]);