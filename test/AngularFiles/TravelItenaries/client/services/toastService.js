app.service('ToastService',['$mdToast',function($mdToast){
    this.showBottomRightToast = function(text){
        $mdToast.show(
            $mdToast.simple()
              .textContent(text)
              .position('bottom right')
              .hideDelay(3000)
          );
    };

}]);