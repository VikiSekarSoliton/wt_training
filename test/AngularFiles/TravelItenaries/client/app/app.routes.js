app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "../views/mainView.html"
    })
    .when("/travelPlan", {
        templateUrl : "../views/travelPlan.html",
        controller : 'travelPlanCtrl'
    }); 
     
});