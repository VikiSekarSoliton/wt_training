app.directive("initMap", ['$window','$document',function($window,$document) {
    return {
        restrict: "E",
        templateUrl: '../directives/mapView.html',
        link: function(scope,elem,attrs) {
            var jElem = elem.find('div')[0];
            scope[attrs.map] = new google.maps.Map(jElem, {
                center: {lat: 11.0168, lng: 76.9558},
                zoom: 8
            });

            scope.autocomplete = new google.maps.places.Autocomplete(
             (elem.find('input')[0]));

            scope.places = new google.maps.places.PlacesService(scope.map);
            scope.autocomplete.addListener('place_changed', onPlaceChanged);

            function onPlaceChanged() {
                var place = scope.autocomplete.getPlace();
                if (place.geometry) {
                  scope.map.panTo(place.geometry.location);
                  scope.map.setZoom(15);
                  search();
                } else {
                    elem.find('input')[0].placeholder = 'Enter a city';
                }
              }
        }
    };
}]);