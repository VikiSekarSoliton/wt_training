
app.controller('mainCtrl',['$scope','ErrorMsgService','ToastService','NavigationService',
'TripService',
function($scope,ErrorMsgService,ToastService,NavigationService,TripService){

    $scope.primaryInfo = {};
    $scope.nameInputErrors = ErrorMsgService.nameInputErrors;
    $scope.currentNavItem = 'addATrip';
    
    $scope.addTrip = function(){
        var isAlreadyAdded = false;
       TripService.trips.forEach(function(element,i,elements){
            if(element.primaryInfo.tripName == $scope.primaryInfo.tripName){
                isAlreadyAdded = true;
            }
        });

        if(isAlreadyAdded){
            ToastService.showBottomRightToast("You've already added this trip!!");
        }else{
            console.log("Adding Trip.." , TripService.trips);
            TripService.trips.push({primaryInfo:angular.copy($scope.primaryInfo)});
            NavigationService.goTo('planYourTravel','travelPlan',$scope);
        } 
    };

}]);


